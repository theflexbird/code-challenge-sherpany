import * as React from "react";
import UserDetails from "../../components/userDetails";
import * as ShallowRenderer from "react-test-renderer/shallow";

const props = {
  open: true,
  setOpen: () => {},
  item: {
    email: "mail@mail.com",
    login: {
      username: "899asdfWall"
    },
    location: {
      city: "LA",
      street: "Wall Street",
      postcode: "CA-1234",
      state: "CA"
    }
  }
};

it("renders correctly", () => {
  const renderer = ShallowRenderer.createRenderer();
  expect(renderer.render(<UserDetails {...props} />)).toMatchSnapshot();
});
