import * as React from "react";
import UserCard from "../../components/userCard";
import * as ShallowRenderer from "react-test-renderer/shallow";

const props = {
  item: {
    email: "mail@mail.com",
    login: {
      username: "899asdfWall"
    },
    name: {
      title: "mr",
      first: "Matt",
      last: "Kowalski"
    },
    picture: {
      thumbnail: "http://someurl.com"
    },
    location: {
      city: "LA",
      street: "Wall Street",
      postcode: "CA-1234",
      state: "CA"
    }
  }
};

it("renders correctly", () => {
  const renderer = ShallowRenderer.createRenderer();
  expect(renderer.render(<UserCard {...props} />)).toMatchSnapshot();
});
