import * as React from "react";
import CatalogMessageBox from "../../components/catalogMessageBox";
import * as ShallowRenderer from "react-test-renderer/shallow";

const props = {
  message: "Example test"
};

it("renders correctly", () => {
  const renderer = ShallowRenderer.createRenderer();
  expect(renderer.render(<CatalogMessageBox {...props} />)).toMatchSnapshot();
});
